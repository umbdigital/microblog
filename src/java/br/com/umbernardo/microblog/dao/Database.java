/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.umbernardo.microblog.dao;

/**
 *
 * @author umbernardo
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Database {
    
    private String url = "jdbc:derby://localhost:1527/blogdb";
    private String username = "root";
    private String password = "root";
    
    
    public Connection getConnection() throws SQLException{
        return DriverManager.getConnection (url, username, password);
    }
}
