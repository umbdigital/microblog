
package br.com.umbernardo.microblog.dao;


import br.com.umbernardo.microblog.models.Post;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aluno
 */

public class PostDAO {
    private Connection conn;
    
    public PostDAO() throws SQLException{
        Database db = new Database();
        this.conn = db.getConnection();
    }
    // este metodo sera chamado logo antes da destruição da instncia desta classe
    @Override
    public void finalize(){
        try {
            this.conn.close();
        } catch (SQLException ex) {
            //Logger.getLogger(PostDAO.class.getTitulo()).log(Level.SEVERE, null, ex);
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void insert(Post post) throws SQLException{
        String sql = "INSERT INTO tbPOST(titulo, descricao) VALUES(?, ?)";
        
        PreparedStatement stmt = this.conn.prepareStatement(sql);
        stmt.setString(1, post.getTitulo());
        stmt.setString(2, post.getDescricao());
        
        stmt.executeUpdate();
        stmt.close();
    }
    
    public ArrayList<Post> findAll() throws SQLException{
        String sql = "SELECT * FROM tbPOST";
        PreparedStatement stmt = this.conn.prepareStatement(sql);
        
        ResultSet rs = stmt.executeQuery();
        
        ArrayList<Post> list = new ArrayList<>();
        
        while (rs.next()){
            String titulo = rs.getString("titulo");
            String descricao = rs.getString("descricao");
            
            Post post = new Post(titulo, descricao);
            list.add(post);
        }
        
        stmt.close(); // ao fazer isso tambem fecha o resultset
        
        return list;
    }
    
}
