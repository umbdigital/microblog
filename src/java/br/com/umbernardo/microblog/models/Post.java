/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.umbernardo.microblog.models;

/**
 *
 * @author umbernardo
 */
public class Post {
    private String titulo;
    private String descricao;
    
    public Post(){
    }
    
    public Post(String titulo, String descricao){
        this.titulo = titulo;
        this.descricao = descricao;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescricao() {
        return descricao;
    }
}
