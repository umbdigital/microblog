/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.umbernardo.microblog.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import br.com.umbernardo.microblog.models.Post;
import br.com.umbernardo.microblog.dao.PostDAO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.registry.infomodel.User;

/**
 *
 * @author umbernardo
 */

@WebServlet(name = "PostNew", urlPatterns = {"/post/new.do"})
public class PostNew extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PrintWriter out = resp.getWriter();
            out.println("<h1>Testando</h1>");
            
            String titulo = req.getParameter("titulo");
            String descricao = req.getParameter("descricao");
            
            Post post = new Post(titulo,descricao);
            PostDAO dao = new PostDAO();
            dao.insert(post);
            
            req.setAttribute("post", post);
            
            RequestDispatcher disp =  req.getRequestDispatcher("/show.jsp");
            disp.forward(req, resp);
        } catch (SQLException ex) {
            resp.sendRedirect("error.jsp");
        }
    }
    
}