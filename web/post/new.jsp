<%-- 
    Document   : addPost
    Created on : 28/09/2016, 11:17:51
    Author     : umbernardo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp include page ="_header.jsp" />

        <h2>Adicionar Post</h2>
        <form action="new.do">
            <label>Informe o título</label><br>
             <input type="text" name="titulo"><br><br>
             <label>Informe a descrição</label><br>
            <textarea rows="4" cols="40" name="descricao"></textarea><br><br>
            <button type="submit" name="cadastrarPost">Cadastrar</button>
        </form>
        <br><br><a href="../index.jsp">Voltar ao menu</a>
        
<jsp include page ="_footer.jsp" />